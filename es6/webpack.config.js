module.exports = {
    context: __dirname,
    entry: "./script.js",
    output: {
        path: __dirname,
        filename: "dist.js"
    },
    module: {
        loaders: [
            { test: /\.js|\.jsx/, loader: "babel" },
            { test: /\.css$/, loader: "style!css" }
        ]
    }
};